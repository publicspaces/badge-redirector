# PublicSpaces Badge redirect 

Simple redirection script to redirect visitors from the PublicSpaces badge to the right PublicSpaces profile page.


## Usage 

Clone this repo and run:

```
API_URL=https://url-to-the-backoffice-api API_KEY=secret API_SECRET=alsosecret go run .
```

Build using:
```
go build .
```


## docker 

Build a docker image

```
docker build -t badge-redirect . 
```

Run using:
```
docker run \
    --env API_URL=https://url-to-the-backoffice-api \ 
    --env API_KEY=secret \
    --env API_SECRET=alsosecret \
	badge-redirect
```

