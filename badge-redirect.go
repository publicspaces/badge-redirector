package main

import (
    "encoding/json"
    "log"
    "net/http"
    "os"
    "fmt"
    "io/ioutil"
    "time"
)

// defining the structs
type Result struct {
    Message struct {
        Redirect string `json:"redirect"`
    }   `json:"message"`
}


func main() {

    //log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
    log.SetOutput(os.Stdout)
    log.Printf(fmt.Sprintf("API URL: %s?r=%s",os.Getenv("API_URL"),"domain.example"))

    http.HandleFunc("/", redirect)
    err := http.ListenAndServe(":9090", nil)
    if err != nil {
        log.Fatal("ListenAndServe: ", err)
    }
}

func redirect(w http.ResponseWriter, r *http.Request) {
    

    // setup http client
    client := &http.Client{
        Timeout: time.Second * 3, // Timeout after 2 seconds
    }

    domain := r.URL.Query().Get("r")

    url :=      fmt.Sprintf("%s?r=%s",os.Getenv("API_URL"),domain)
    token :=    fmt.Sprintf("token %s:%s",os.Getenv("API_KEY"),os.Getenv("API_SECRET"))
    req, err := http.NewRequest("GET", url, nil)
    req.Header.Add("Authorization", token)

    resp, err := client.Do(req)
    if err != nil {
       log.Fatalln(err)
    }
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body) // response body is []byte

    var result Result
    if err := json.Unmarshal(body, &result); err != nil {  // Parse []byte to the go struct pointer
        fmt.Println("Can not unmarshal JSON")
    }

    log.Printf("%s %s %s -> %s\n", r.RemoteAddr, r.Method, r.URL,result.Message.Redirect)

    
    http.Redirect(w, r, result.Message.Redirect, 301)
}
